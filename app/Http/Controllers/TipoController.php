<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class TipoController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\Category);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('Buscar');
			$this->filter->reset('Reiniciar');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('code', 'Code');
			$this->addStylesToGrid();

        */

			$this->grid = \DataGrid::source(new \App\Tipo);
			$this->grid->add('id', 'ID', true);
			$this->grid->orderBy('id','asc'); //default orderby

			$this->grid->add('tipo', 'Nombres');

			$this->grid->add('tipo_desc', 'Descripción');

			$this->grid->add('pertenece_a', 'Pertenece a');

			//$this->grid->orderBy('id','desc'); //default orderby

			$this->addStylesToGrid();
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\Tipo());

			$this->edit->label('Editar Tipos de Pedido');

			$this->edit->add('tipo', 'Nombre', 'text')->rule('required');

			$this->edit->add('tipo_desc', 'Descripción', 'text')->rule('required');

			$this->edit->add('pertenece_a', 'Pertenece a', 'text')->rule('required');
		
       
        return $this->returnEditView();
    }    
}
