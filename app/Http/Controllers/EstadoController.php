<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class EstadoController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        // Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\Estado);

			/*$this->grid = \DataGrid::source(new \App\Tipo);
			$this->grid->add('id', 'ID', true);
			$this->grid->orderBy('id','asc');*/


			$this->filter->add('nombre', 'Nombre', 'text');
			$this->filter->submit('Buscar');
			$this->filter->reset('Reiniciar');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('id', 'ID');
			$this->grid->add('nombre', 'Nombre');
			$this->grid->add('orden', 'Orden');

			$this->grid->add('pertenece_a', 'Pertenece a');
			$this->addStylesToGrid();

        
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        // Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\Estado());

			$this->edit->label('Editar Estado');

			// 'id', 'nombre', 'estado', 'mensaje', 'modo', 'flag', 'orden', 'propiedades'

			$this->edit->add('nombre', 'Nombre', 'text')->rule('required');
		
			$this->edit->add('estado', 'Código', 'text')->rule('required');

			$this->edit->add('pertenece_a', 'Pertenece a', 'text')->rule('required');

			//$this->edit->add('mensaje', 'Mensaje', 'textarea');

			$this->edit->add('modo', 'Mensaje', 'text');

			$this->edit->add('orden', 'Orden', 'text')->rule('required');

			$this->edit->add('propiedades', 'Propiedades', 'textarea');

			        
       
        return $this->returnEditView();
    }   
}
