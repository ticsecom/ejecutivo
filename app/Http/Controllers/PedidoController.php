<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;
use DB;

class PedidoController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        	$this->filter = \DataFilter::source(\App\Pedido::with('asignado','estado','tipo'));

		
			$this->filter->add('fecha', 'Fecha', 'daterange')->format('Y/m/d', 'es');


			$options = \App\Tipo::all()->lists("tipo", "id")->all();
			$blank_option = array(""=>"Tipo");
			$tipo_options = $blank_option + $options;

			$this->filter->add('tipo_id','Tipo','select')->insertValue(0)->options($tipo_options);

			$this->filter->add('tema', 'Tema', 'text');


			$options = \App\Estado::where('pertenece_a', "Pedidos")->lists("nombre", "id")->all();
			$blank_option = array(""=>"Estado");
			$estado_options = $blank_option + $options;

			$this->filter->add('estado_id','Estado','select')->insertValue(0)->options($estado_options);

			$this->filter->submit('Buscar');
			$this->filter->reset('Borrar');
			$this->filter->build();


			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('id', 'ID');
			$this->grid->add('fecha', 'Fecha');
			$this->grid->add('tema', 'Tema');
			$this->grid->add('con_copia', 'Correo con Copia');
			
			$this->grid->add('asignado.email', 'Asignado a');
			$this->grid->add('directrices', 'Directrices Secretario');
			//ultima accion

			$this->grid->add('estado.nombre', 'Estado');
			$this->grid->add('observaciones', 'Observaciones');


			

			$this->addStylesToGrid();
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);
        \App\Pedido::creating(function($data) {
	    	//$data->alerta_id_aux = \Request::input('alerta_id');
	    	$admin_id = DB::table('admins')->where("email",\Session::get('admin_email'))->value('id');
	    	$data->admin_id = $admin_id;
		});		

        $this->edit = \DataEdit::source(new \App\Pedido());

	    $this->edit->label('Editar Pedido');

	    $this->edit->add('tipo_id','Tipo','select')->options(\App\Tipo::all()->lists("tipo", "id")->all())->rule('exists:pedido_tipo,id')->rule('required');

	    $this->edit->add('fecha', 'Fecha', 'date')->format('Y/m/d', 'es')->rule('required');


	    $this->edit->add('tema', 'Título', 'text')->rule('required');
	    $this->edit->add('correo', 'Correo', 'textarea')->rule('required');
	    $this->edit->add('con_copia', 'Con Copia', 'text')->rule('required');



	    

	    $options = \Serverfireteam\Panel\Admin::all()->lists("email", "id")->all();
			array_unshift($options, '');

	    $this->edit->add('asignado_a','Responsable','select')->options($options);

	    $this->edit->add('directrices', 'Directrices Secretario Nacional', 'textarea');

	    $this->edit->add('observaciones', 'Observaciones', 'textarea');


	   /* $options = \App\Correo::all()->lists("tema", "id")->all();
			array_unshift($options, '');

	    $this->edit->add('correo_id','Correo en el que se originó','select')->options($options);*/


	    $this->edit->add('estado_id','Estado Actual','select')->options(\App\Estado::all()->lists("nombre", "id")->all())->rule('exists:estados,id')->rule('required');

	    //$options = \App\Estado::where('pertenece_a', "Pedidos")->lists("nombre", "id")->all();
			//array_unshift($options, '');
	     // $this->edit->add('estado_id','Estado Actual','select')->options(\App\Tipo::all()->lists("estado", "id")->all())->rule('exists:estado,id')->rule('required');

	      // $this->edit->add('tipo_id','Tipo','select')->options(\App\Tipo::all()->lists("tipo", "id")->all())->rule('exists:estado,id')->rule('required');

        // $this->edit->add('estado_id','Estado Actual','select')->options($options)->rule('exists:estado,id')->rule('required');

        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\Category());

			$this->edit->label('Edit Category');

			$this->edit->add('name', 'Name', 'text');
		
			$this->edit->add('code', 'Code', 'text')->rule('required');


        */
       
        return $this->returnEditView();
    }    
}
