<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;
use DB;

class AccionController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        
		$this->filter = \DataFilter::source(\App\Accion::with('admin' ,'pedido')); 

		$this->filter->add('fecha', 'Fecha', 'daterange')->format('Y/m/d', 'es');
		$this->filter->add('titulo', 'Título Acción', 'text');

		$this->filter->submit('Buscar');
		$this->filter->reset('Borrar');
		$this->filter->build();

		$this->grid = \DataGrid::source($this->filter);
		$this->grid->add('id', 'ID');
		$this->grid->add('titulo', 'Título');
		$this->grid->add('descripcion', 'Descripción Acción');
		$this->grid->add('pedido.tema', 'Nombre Pedido');
		$this->grid->add('admin.email', 'Asignado a');
		$this->grid->add('updated_at', 'Ultima Modif.');
		
		$this->addStylesToGrid();
		



                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        \App\Accion::creating(function($data) {
	    	//$data->alerta_id_aux = \Request::input('alerta_id');
	    	$admin_id = DB::table('admins')->where("email",\Session::get('admin_email'))->value('id');
	    	//dd($admin_id);
	    	$data->admin_id = $admin_id;
		});	

		$this->edit = \DataEdit::source(new \App\Accion());
		$this->edit->label('Editar Acción');

		
		$options = \App\Pedido::all()->lists("tema", "id")->all();
		$blank_option = array(""=>"Pedido");
		$pedido_options = $blank_option + $options;

		$this->edit->add('pedido_id','Pedido','select')->insertValue(0)->options($pedido_options);



		$this->edit->add('titulo', 'Titulo', 'text')->rule('required');
		$this->edit->add('descripcion', 'Descripción', 'text')->rule('required');

		$options = \Serverfireteam\Panel\Admin::all()->lists("email", "id")->all();
			array_unshift($options, '');

	    $this->edit->add('admin_id','Responsable','select')->options($options);




       
        return $this->returnEditView();
    }    
}
