<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model {

    protected $table = 'pedidos';


   /* public function correo() {
        return $this->belongsTo(Correo::class, 'correo_id');
    }
*/
     /**
     * Get the status associated with the pedido.
     */
    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

    /**
     * Get the type associated with the pedido.
     */
    public function tipo()
    {
        return $this->belongsTo(Tipo::class, 'tipo_id');
    }

    public function admin() {

        return $this->belongsTo(\Serverfireteam\Panel\Admin::class, 'admin_id');
    }

    /*public function accion() {
        return $this->belongsToMany(Accion::class);
    }*/

    public function asignado() {
        return $this->belongsTo(\Serverfireteam\Panel\Admin::class, 'asignado_a');
    }

}
