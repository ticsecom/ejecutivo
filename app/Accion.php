<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accion extends Model {

    protected $table = 'acciones';

    public function admin() {

        return $this->belongsTo(\Serverfireteam\Panel\Admin::class, 'admin_id');

     }

    
    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'pedido_id');
    }

}
