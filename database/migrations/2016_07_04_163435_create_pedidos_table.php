<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');

            //$table->integer('prioridad_id')->unsigned()->nullable()->index('prioridad_id');
            $table->integer('tipo_id')->unsigned()->nullable()->index('tipo_id');

            $table->string('tema');
            $table->string('correo', 65535)->nullable();
            $table->string('con_copia');
            $table->integer('admin_id')->unsigned()->nullable()->index('admin_id');
            $table->integer('asignado_a')->unsigned()->nullable()->index('asignado_a');

            $table->text('directrices', 65535)->nullable();

            $table->text('observaciones', 65535)->nullable();

            //$table->integer('correo_id')->unsigned()->nullable()->index('correo_id');
            $table->integer('estado_id')->unsigned()->nullable()->index('estado_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedidos');
    }
}
