<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('descripcion', 65535)->nullable();


            $table->integer('pedido_id')->unsigned()->nullable()->index('pedido_id');
            $table->integer('admin_id')->unsigned()->nullable()->index('admin_id');


            $table->timestamps();
        });

        // Schema::create('accion_pedido', function (Blueprint $table) {
        //     $table->integer('accion_id')->unsigned()->index();
        //     $table->foreign('accion_id')->references('id')->on('acciones')->onDelete('cascade');

        //     $table->integer('pedido_id')->unsigned()->index();
        //     $table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade');
            
        //     $table->timestamps();
        // });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acciones');
        // Schema::drop('accion_acciones');
    }
}
