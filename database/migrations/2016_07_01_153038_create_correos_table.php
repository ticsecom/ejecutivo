<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorreosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correos', function (Blueprint $table) {
            $table->increments('id');
             $table->date('fecha');
             $table->string('para');
             $table->string('cc');
             $table->string('asunto');
             $table->text('contenido', 65535)->nullable();

             $table->integer('estado_id')->unsigned()->nullable()->index('estado_id');
             $table->integer('admin_id')->unsigned()->nullable()->index('admin_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correos');
    }
}
